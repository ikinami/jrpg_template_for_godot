# Godot 4 JRPG Template

This is a simple template for overworld movement in a JRPG made for Godot 4.

Uses custom data layers in the tilemap for easy editing!

Check [the demo video](godot_jrpg.mp4) for basic usage!

Made with ❤️ by [Kinami Imai 今井きなみ](https://ikinami.gitlab.io/kinami/)
