extends Sprite2D

func _input(event):
	if event is InputEventKey and event.is_pressed():
		var direction = Vector2i(
			Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
		)
		if get_parent().try_move(self, direction):
			position += Vector2(get_parent().tile_set.tile_size * direction)
