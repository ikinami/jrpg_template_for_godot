extends TileMap

@export var audio_tracks : Array[Resource]

func try_move(node, direction):
	if direction.length() == 0:
		return
	var target = get_cell_tile_data(0, local_to_map(node.position)+direction)
	$audio.stream = audio_tracks[target.custom_data_1]
	$audio.play()
	if target.custom_data_2 != NodePath(""):
		get_node(target.custom_data_2).interact()
	if target.custom_data_0:
		return false
	else:
		return true
